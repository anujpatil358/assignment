import tweepy
import os
import socket
import json
from dotenv import load_dotenv
from tweepy.streaming import StreamListener
from tweepy import Stream
from tweepy.auth import OAuthHandler

load_dotenv()

consumer_token = os.getenv('CONSUMER_KEY')
consumer_secret = os.getenv('CONSUMER_SECRET')
ACCESS_TOKEN = os.getenv('ACCESS_TOKEN')
ACCESS_SECRET = os.getenv('ACCESS_SECRET')


# override tweepy.StreamListener to add logic to on_status
class MyStreamListener(StreamListener):
    def __init__(self, csocket):
        super().__init__()
        self.client_socket = csocket

    def on_data(self, raw_data):
        try:
            tweets = json.loads(raw_data)
            print(tweets['text'].encode('utf-8'))
            self.client_socket.send(str(tweets['text']+ "\n").encode('utf-8'))
            return True
        except BaseException as e:
            print(f'On data function error {e}')
        return True

    def on_error(self, status):
        print('status----', status)
        return True


def get_tweet(conn):
    auth = OAuthHandler(consumer_token, consumer_secret)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)
    myStream = Stream(auth=auth, listener=MyStreamListener(conn))
    myStream.filter(track=['python'], languages=["en"], locations=[68.11, 6.55, 97.4, 35.67])


if __name__ == '__main__':
    # search_url = "https://api.twitter.com/2/tweets/search/recent?query=india"
    sock = socket.socket()
    host = '127.0.0.1'
    port = 9000
    sock.bind((host, port))
    print(f'listing on port {port}')

    sock.listen(5)
    conn, addr = sock.accept()

    print(f'Received request from {addr}')
    get_tweet(conn)
