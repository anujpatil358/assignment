"""
Assignment 2
    Create a pyspark script which will read data from particular table from Mysql/Postgres database
    Perform some operations/transformation on the data and save the result to local file storage
    Schedule Spark command in Airflow
    Which will run every day at specific time
    On failure/retry you should get an email
    should have 2 retries
"""
from pyspark.sql import SparkSession
from pyspark.sql.functions import col


def read_data():
    spark_session = SparkSession.builder.appName('assignment2') \
        .master("local") \
        .config("spark.jars", "D:/Office/NeoSoft/Projects/postgres config/postgresql-42.2.23.jar") \
        .getOrCreate()

    movie_csv = 'D:/Office/NeoSoft/Projects/Assignment/Code/Third/highest_rating_movies.csv'
    # bulk_upload = spark_session.sql("COPY public.movie FROM " + movie_csv + " WITH CSV HEADER")

    df = spark_session.read \
        .format("jdbc") \
        .option("url", "jdbc:postgresql://localhost:5432/assignment2") \
        .option("dbtable", 'movie') \
        .option("user", "postgres") \
        .option("password", "1234") \
        .option("driver", "org.postgresql.Driver") \
        .load()

    df.show(truncate=False)
    print('=' * 200)
    comedy_movie = df.filter(col("genres").contains("Comedy"))
    comedy_movie.show(truncate=False)


if __name__ == '__main__':
    read_data()
