"""
Assignment 1
    Create a DAG which will run every hour
    Schedule the above script in DAG
    On failure/retry you should get an email
    should have 2 retries
"""

import airflow
from airflow import DAG
from airflow.operators.python_operator import PythonOperator

from datetime import timedelta
from ..second_main import read_data

default_args = {
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(2),
    # 'end_date': datetime(2018, 12, 30),
    'depends_on_past': False,
    'email': ['anuj.patil@neosoftmail.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 2,
    'retry_delay': timedelta(minutes=1),
}

with DAG(
        'read data from particular table',
        default_args=default_args,
        schedule_interval='0 * * * *',
) as dag:
    generate_file = PythonOperator(
        task_id="1",
        python_callable=read_data,
    )
