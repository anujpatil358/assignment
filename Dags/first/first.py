"""
Assignment 1
    Create a Python script which downloads CSV file from a particular email id. It should filter based on From
    and subject and save the file to the local directory.
"""

import imaplib
import pprint
import os
import email
from dotenv import load_dotenv

load_dotenv()


def subject_to_folder(text):
    # clean text for creating a folder
    return "".join(c if c.isalnum() else "_" for c in text)


# script which downloads CSV file from a particular email id
def download_csv_from_email():
    imap_host = os.getenv('imap_host')
    imap_user = os.getenv('imap_user')
    imap_pass = os.getenv('imap_pass')

    # connect to host using SSL
    imap = imaplib.IMAP4_SSL(imap_host)
    # login to server
    imap.login(imap_user, imap_pass)

    imap.select('Inbox')

    # FILTER MAIL BASED ON SUBJECT AND EMAIL
    _, data = imap.search(None, '(FROM "anuj.patil@neosoftmail.com" SUBJECT "testing with csv")')

    data = data[0].decode("utf-8")
    data = data.split()
    _, msg = imap.fetch(data[0], '(RFC822)')

    msg = msg[0][1].decode("utf-8")
    message = email.message_from_string(msg)
    pprint.pprint(dir(message))
    print(message.get_boundary())
    subject = message['subject']
    from_email = message['from']
    to_email = message['to']

    print("Subject=", subject)
    print("From email=", from_email)
    print("To email=", to_email)

    if message.is_multipart():
        for msg in message.walk():
            filename = msg.get_filename()
            print(msg.get_payload(decode=True))
            if filename:
                folder_name = subject_to_folder(subject)
                att_path = os.path.join(folder_name, filename)
                try:
                    os.mkdir(os.path.join(folder_name))
                except Exception as e:
                    pass
                if not os.path.isfile(att_path):
                    fp = open(att_path, 'wb')
                    fp.write(msg.get_payload(decode=True))
                    fp.close()

#
# if __name__ == '__main__':
#     download_csv_from_email()


